      
import './App.css';
import Topbar from './component/topbaar/Topbar';
import Sidebar from './component/sidebar/sidebar'
import Home from './pages/Home/Home'
import { BrowserRouter as Router , Routes, Route} from 'react-router-dom';
import CustomizedTables from './pages/user/user';
import Edituser from './component/editUser/editButton';
import Registration from './pages/Registration/registration';
import Loginpage from './pages/loginpage/loginpage';
import { useEffect, useState } from 'react';
import Loading from './pages/loadingshow/loading';

function App() {

 

  let[show,setshow]=useState()

  useEffect(() => {
    setshow(localStorage.user)
  },[show])


  return (    
    <>  
    {show?<>
<Topbar />
        <div className='container'>
        <Sidebar  className='sideBar' />  
          <Routes >
            <Route path='/home' element={<Home />} />
            <Route path='/user' element={<CustomizedTables/>} />
            <Route path='/edituser/:id'  element={<Edituser />} />
            <Route path='/registration' element={<Registration />} />
            <Route path='/loading' element={<Loading />} />
            </Routes> 
        </div>
      </>:<Loginpage />}
    </>
  );
}

export default App;
