/* eslint-disable react/jsx-no-duplicate-props */
/* eslint-disable no-redeclare */
/* eslint-disable jsx-a11y/anchor-has-content */
/* eslint-disable jsx-a11y/anchor-is-valid */
import "./loginpage.css";
import React, { useEffect, useState } from "react";
import { useNavigate } from 'react-router-dom';


function Loginpage() {
  let [email, setemail] = useState();
  let [password, setpassword] = useState();
  let navigate = useNavigate();

  // console.log(navigate,"navigate");

  // useEffect(() => {
  //   navigate('/loading')
  // },[email])

  
 
  const Loginbutton = (e) => {
    e.preventDefault();
    if (email === "shubhambisht0123@gmail.com" && password === "1234") {
      
      navigate('/loading')
      localStorage.setItem("user", true);
      window.location.reload()
    }
  };

  return (
    <>

      <div className="box-body">
        <div className="top_box">
          <form className="box-form" onSubmit={Loginbutton}>
            <div className="box-title">
              <h2>Login</h2>
            </div>
            <div className="box-inputs">
              <div className="inputs-email">
                <input
                  type="email"
                  name="email"
                  id="email"
                  className="input"
                  onChange={(e) => setemail(e.target.value)}
                  placeholder="Type your e-mail"
                />
                <label className="label-inputs">E-mail:</label>
              </div>

              <div className="inputs-password">
                <input
                  type="password"
                  name="password"
                  id="password"
                  className="input"
                  onChange={(e) => setpassword(e.target.value)}
                  placeholder="Type your password"
                />
                <label className="label-inputs">Password:</label>
              </div>

              <div className="inputs-remember">
                <input type="checkbox" name="remember" id="remember" checked />
                <label>Remember me</label>
              </div>

              <div className="box-btn-login">
                <button
                  className="btn-login"
                  type="submit"
                  // onClick={Loginbutton}
                  value="enter"
              
                >
                  Enter
                </button>
              </div>
            </div>

            <div className="box-register">
              <p>
                Forgot password? <a href="#">Click here!</a>
              </p>
              <p>
                Don't have an account? <a href="#">Create one!</a>
              </p>
            </div>

            <div className="break-line">
              <p>ou</p>
            </div>

            <div className="box-midias">
              <div className="midia">
                <a href="#" className="link-midias">
                  <ion-icon name="logo-google"></ion-icon>
                </a>
              </div>

              <div className="midia">
                <a href="#" className="link-midias">
                  <ion-icon name="logo-github"></ion-icon>
                </a>
              </div>

              <div className="midia">
                <a href="#" className="link-midias">
                  <ion-icon name="logo-facebook"></ion-icon>
                </a>
              </div>
              <div className="midia">
                <a href="#" className="link-midias">
                  <ion-icon name="logo-codepen"></ion-icon>
                </a>
              </div>
            </div>
          </form>
          <div className="hero">
            <p>Welcome to</p>
            <h1>Admil Panel</h1>
          </div>
        </div>
      </div>
    </>
  );
}

export default Loginpage;
