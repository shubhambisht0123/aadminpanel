/* eslint-disable no-template-curly-in-string */
/* eslint-disable jsx-a11y/img-redundant-alt */
import './user.css'
import { React, useEffect, useState } from 'react';
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import DeleteIcon from '@mui/icons-material/Delete';
import Button from '@mui/material/Button';
import { Rows } from '../../dummydata/dummydata';
import {Link } from 'react-router-dom';
import axios from 'axios';
 

const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
        fontSize: 14,
    },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
        border: 0,
    },
}));



export default function CustomizedTables() {

    const [data, setdata] = useState([])
    // let [value, setval]=useState([])
    const API='https://619b6c8327827600174455af.mockapi.io/admin-panel'
  
    useEffect(()=>{
        axios.get(API).then((e)=>setdata(e.data))
    },[])

    const userDeleteButton = (id) => {
        setdata(data.filter((item) => item.id !== id))
    }


    return (

        <TableContainer component={Paper}>
            <div className='userTable'>
            <div className='userTableinside'>
                <Table sx={{ minWidth: 700 }} aria-label="customized table">
                    <TableHead>
                        <TableRow>
                            <StyledTableCell align='right'>ID</StyledTableCell>
                            <StyledTableCell align='center'>User</StyledTableCell>
                            <StyledTableCell align='right'>Gender</StyledTableCell>
                            <StyledTableCell align="right">Email</StyledTableCell>
                            <StyledTableCell align="right">Profile</StyledTableCell>
                            <StyledTableCell align="right">State</StyledTableCell>
                            <StyledTableCell align='center'>Action</StyledTableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {data.map((row) => (

                            <StyledTableRow key={row.name}>
                                <StyledTableCell align="right">{row.id}</StyledTableCell>
                                <StyledTableCell component="th" className='myuser' style={{ display: 'flex' }} scope="row">
                                    <img className='userImage' src={row.gender==='Male'?'https://icon-library.com/images/avatar-icon-images/avatar-icon-images-4.jpg':'https://cdn2.vectorstock.com/i/1000x1000/01/66/businesswoman-character-avatar-icon-vector-12800166.jpg'} alt='image not found' />
                                    {row.name}
                                </StyledTableCell>
                                <StyledTableCell align="right">{row.gender}</StyledTableCell>
                                <StyledTableCell align="right">{row.email}</StyledTableCell>
                                <StyledTableCell align="right">{row.profile}</StyledTableCell>
                                <StyledTableCell align="right">{row.address}</StyledTableCell>
                                <StyledTableCell align="right">
                                    <span className='button'>
                                        
                                            <Button variant="contained" href="#contained-buttons">
                                        <Link to={`/edituser/${row.id}`}  className='editButton'  >  Edit </Link>
                                            </Button>
                                            
                                        <button className='deleteButton' onClick={() => userDeleteButton(row.id)}>
                                            <DeleteIcon />
                                        </button>
                                    </span>
                                </StyledTableCell>
                            </StyledTableRow>
                        ))}
                    </TableBody>
                </Table>
                </div>
            </div>
        </TableContainer>


    );
}