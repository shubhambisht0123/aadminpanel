import axios from "axios";
import React, { useState } from "react";
import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import "./registration.css";


export default function Registration() {
  let [myname, setname] = useState();
  let [myprofile, setprofile] = useState();
  let [mymail, setemail] = useState();
  let [myphone, setphone] = useState();
  let [mygender, setgender] = useState();
  let [mycountry, setcountry] = useState();
  let [myalert, setalert] = useState();
 

  let [radiobtn,setradiobtn]=useState(false);
  console.log(mygender)
  const handleReset=()=>{
          
    setprofile('');
    setname('');
    setemail('');
    setphone('');
    setgender('');
    setcountry('');
  
       setradiobtn(false)
   
   

    setalert(
      <> <Alert severity="success">
    <AlertTitle>Success</AlertTitle>
    This is a successfully sumbit — <strong>check it out!</strong>
  </Alert> 
  </>
    )
    setTimeout(()=>{
setalert('')
    },2000)
  }

  const Button = (e) => {
    e.preventDefault(); 
  document.getElementById('myform').reset()

    axios.post(`https://619b6c8327827600174455af.mockapi.io/admin-panel`, {
      name: myname,
      gender: mygender,
      profile: myprofile,
      email: mymail,
      phone: myphone,
      address: mycountry,
    }).then((res)=>
     handleReset(),    
    )
 
 };

  return (
    <>
    
      <div className="form_wrapper">
        <div className="form_container">
          <div className="title_container">
            <h2 style={{color:'#5550bd', fontWeight:'bold'}}> Registration Form</h2>
          </div>
         {myalert}
          <div className="row clearfix">
            <div className="">
              <form id='myform' onSubmit={Button}>
                <div className="row clearfix">
                  <div className="input_field">
                    <input
                      type="text"
                      name="name"
                      value={myname}
                      onChange={(e) => {
                        setname(e.target.value);
                      }}
                      placeholder="Name"
                      required
                    />
                  </div>
                  <div className="input_field">
                    <input
                      type="text"
                      name="profile"
                      value={myprofile}
                      onChange={(e) => {
                        setprofile(e.target.value);
                      }}
                      placeholder="Profile"
                      required
                    />
                  </div>
                  <div className="input_field">
                    <input
                      type="email"
                      name="email"
                      value={mymail}
                      onChange={(e) => {
                        setemail(e.target.value);
                      }}
                      placeholder="Email"
                      required
                    />
                  </div>
                  <div className="input_field">
                    <input
                      type="text"
                      name="phone"
                      value={myphone}
                      onChange={(e) => {
                        setphone(e.target.value);
                      }}
                      placeholder="Phone  Number"
                      required
                    />
                  </div>
                </div>
                <div className="input_field radio_option">
                  <input
                    type="radio"
                    name="radiogroup1"
                    id="rd1"
                    value='Male'
                    onChange={(value) => setgender(value.target.value)}
                    // checked={mygender}
                  />
                  <label for="rd1">Male</label>

                  
                  <input
                    type="radio"
                    name="radiogroup1"
                    id="rd2"
                    value='Female'
                    onChange={(value) => setgender(value.target.value)}
                    // checked={mygender}
                  />
                  <label for="rd2">Female</label>
                </div>
                <div className="input_field select_option">
                  <select value={mycountry} onChange={(e) => setcountry(e.target.value)}>
                    <option>Select a country</option>
                    <option>INDIA</option>
                    <option>UAS</option>
                    <option>AFGHANISTAN</option>
                    <option>BELIZE</option>
                    <option>AUSTRALIA</option>
                  </select>
                  <div className="select_arrow"></div>
                </div>
                <div className="input_field checkbox_option">
                  <input required  type="checkbox"  onChange={(e) => setradiobtn(e.target.checked)} checked={radiobtn} id="cb1" />
                  <label for="cb1">I agree with terms and conditions</label>
                </div>
               
                <input className="button"  type="submit"    value="Reset form" />
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
