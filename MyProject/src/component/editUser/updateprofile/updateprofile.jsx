/* eslint-disable jsx-a11y/img-redundant-alt */
  
    import React from 'react'
  import './updateprofile.css'  
  import UploadFileIcon from '@mui/icons-material/UploadFile';

    export default function Updateprofile() {
        return (<>
            <div className='updateRight'>
                <div className='formtable'>
                    <h2>Edit</h2>
                    <form>
                    <div className='formtableRow'>
                    <label className='formLabel' >Username</label>
                        <input type='text' placeholder='shubham0123' />
                    </div>

                    <div className='formtableRow'>
                    <label className='formLabel' >Full Name</label>
                        <input type='text' placeholder='Shubham Bisht' />
                    </div>

                    <div className='formtableRow'>
                    <label className='formLabel' >Email</label>
                        <input type='text' placeholder='Shubhambisht0123@gmail.com' />
                    </div>

                    <div className='formtableRow'>
                    <label className='formLabel' >Phone</label>
                        <input type='text' placeholder='+91 826 374 8596' />
                    </div>

                    <div className='formtableRow'>
                    <label className='formLabel' >Address</label>
                        <input type='text' placeholder='India' />
                    </div>

                    </form>
                </div>
{/* Upload section */}
                <div className='profileImage'>
                <div className='uploadImg'>
   <img className='Img' src='https://media.istockphoto.com/photos/colored-powder-explosion-on-black-background-picture-id1057506940?k=20&m=1057506940&s=612x612&w=0&h=3j5EA6YFVg3q-laNqTGtLxfCKVR3_o6gcVZZseNaWGk=' alt='Image not found' />
              
              <label htmlFor='file'> <UploadFileIcon /></label>
               <input type='file' id='file' style={{display:'none'}} />
                </div>
  <button className='updateBtn'>Upload</button>
                
          
                </div>
            </div>
            </>
                
        )
    }
    