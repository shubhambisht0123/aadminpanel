/* eslint-disable array-callback-return */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import Avatar from "@mui/material/Avatar";
import "./viewprofile.css";
import PersonOutlineIcon from "@mui/icons-material/PersonOutline";
import CalendarTodayIcon from "@mui/icons-material/CalendarToday";
import CallIcon from '@mui/icons-material/Call';
import MailOutlineIcon from '@mui/icons-material/MailOutline';
import PersonPinIcon from '@mui/icons-material/PersonPin';
import { useParams } from "react-router";
import axios from "axios";


export default function Viewuser(props) {
  let {id}= useParams()
let [apidata,setapidata]=useState(id)
let [adata,setdata]=useState([])
 
useEffect(()=>{ 

  const API=`https://619b6c8327827600174455af.mockapi.io/admin-panel/${apidata}`
  axios.get(API).then((e)=>setdata([e.data]))
},[])



  return (
    <>
      <div className="Viewuser">
        <div className="viewusercard">
       <div style={{padding:'20px 0px'}}><h2>User Profile</h2></div> 

      { adata.map((mydata)=>(

   <> <div className="userNameTemp">
            <Avatar
              alt="Shubham"
              src={mydata.gender==='Male'?'https://icon-library.com/images/avatar-icon-images/avatar-icon-images-4.jpg':'https://cdn2.vectorstock.com/i/1000x1000/01/66/businesswoman-character-avatar-icon-vector-12800166.jpg'}
              sx={{ width: 70, height: 70 }}
            />
            <div className="userName">
              <h4 style={{color:'rgb(131, 129, 200)'}}>{mydata.name}</h4>
              <span style={{color:'#444'}}> {mydata.profile} </span>
            </div>
          </div>
          <div className="userAccount">
            <h4 className='title'>Account Details</h4>

            <div className="accountdet">
              <PersonOutlineIcon  /> <h5>{mydata.gender}</h5>
            </div>

            <div className="accountdet">
              <CalendarTodayIcon />
              <h5>18.11.2021</h5>
            </div>

          </div>

          <div className="userAccount">
            <h4 className='title'>Contect Details</h4>

            <div className="accountdet">
              <CallIcon  /> <h5>+91 {mydata.phone}</h5>
            </div>

            <div className="accountdet">
              <MailOutlineIcon />
              <h5>{mydata.email}</h5>
            </div>

            <div className="accountdet">
              <PersonPinIcon /><h5>{mydata.address}</h5>
            </div>
          </div>
          </>

      ))}

    
  
        </div>
      </div>
    </>
  );
}
