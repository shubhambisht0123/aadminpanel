import "./editButton.css";
// import React, { useEffect, useState } from "react";
import Viewuser from "./viewprofile/viewprofile";
import Updateprofile from "./updateprofile/updateprofile";
// import axios from "axios";


export default function Edituser() {
  // const [data, setdata] = useState([])
  // // let [value, setval]=useState([])
  // const API='https://619b6c8327827600174455af.mockapi.io/admin-panel'

  // useEffect(()=>{
  //     axios.get(API).then((e)=>setdata(e.data))
  // },[])


  return (
    <>
      <div className="editUser">
        <div className="editUserpage">
          <Viewuser  />
          <Updateprofile />
        </div>
      </div>
    </>
  );
}
