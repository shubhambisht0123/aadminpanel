/* eslint-disable jsx-a11y/anchor-has-content */
/* eslint-disable jsx-a11y/img-redundant-alt */

import './Topbar.css'
import NotificationsNoneIcon from '@mui/icons-material/NotificationsNone';
import SettingsIcon from '@mui/icons-material/Settings';
import LanguageIcon from '@mui/icons-material/Language';
import { useNavigate } from 'react-router';

// import { NavLink } from 'react-router-dom';
const Topbar = () => {
 
    // const navigate = useNavigate();
    
     const Logoutbtn =()=>{
        
            //  console.log(navigate);
              
    }

    return (
        <div className='topbar'>
            <div className="topbarWrapper">
                <div className="topLeft">
                    <span style={{ color: 'white', fontSize: "30px", fontWeight: 'bold' }}>Facebook</span>
                </div>
                <div className="topRight">
                    <div className="topbaricons">
                        <NotificationsNoneIcon />
                        <span className="topnotifaction">2</span>
                    </div>

                    <div className="topbaricons">
                        <LanguageIcon />
                        <span className="topnotifaction">2</span>
                    </div>

                    <div className="topbaricons">
                        <SettingsIcon />
                        <span className="topnotifaction">2</span>
                    </div>
                    <div className="topbaricons">
                        <img src='https://lh3.googleusercontent.com/a-/AOh14GjPRhKPMOLgSlcnPMGR3J1iqNVjmBA2L0bYTm3ruQ=s288-p-rw-no' className='imgprofile' alt='image not found' />
                    </div>
                    <div className="topbaricons">
                        <button className='logout-btn' onClick={Logoutbtn} >Logout</button>
                    </div>

                </div>
            </div>


        </div>
    )
}
export default Topbar

