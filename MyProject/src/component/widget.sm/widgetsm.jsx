/* eslint-disable array-callback-return */
/* eslint-disable jsx-a11y/img-redundant-alt */
import React, { useEffect, useState } from "react";
import "./widgetsm.css";
import VisibilityIcon from "@mui/icons-material/Visibility";
import axios from "axios";
import Button from "@mui/material/Button";
import Tooltip from "@mui/material/Tooltip";
import Viesdata from "./viewsmall/viewsmall";


const API = "https://619b6c8327827600174455af.mockapi.io/admin-panel";

export default function Widgetsm() {
  let [data, setdata] = useState([]);
  useEffect(() => {
    axios.get(API).then((e) => setdata(e.data));
  }, []);


  return (
    <>
      <div className="wedgetLeft">
        <div className="wedgeTopbar">
          <h3 className="widgetsmTitles">New Join Members</h3>
        </div>
        {data.map((val) => (
          <ul className="widgetsmUl">
            <li>
              <div className="sircialDiv">
                <img
                  className=" widgetsmImage"
                  src={
                    val.gender === "Male"
                      ? "https://icon-library.com/images/avatar-icon-images/avatar-icon-images-4.jpg"
                      : "https://icons-for-free.com/iconfiles/png/512/female+person+user+woman+young+icon-1320196266256009072.png"
                  }
                  alt="IMAGE not founded"
                />
              </div>
            </li>
            <li className='proname'>
            
                <h2 className="userName" style={{float:'left'}}>{val.name} </h2>
                <h3 className="UserTitle"> {val.profile}</h3>
         
            </li>
            <li>
              <Tooltip title={Viesdata(val)} placement="right">
                <Button>
                  <VisibilityIcon />
                  view
                </Button>
              </Tooltip>
            </li>
          </ul>
        ))}
      </div>
    </>
  );
}
