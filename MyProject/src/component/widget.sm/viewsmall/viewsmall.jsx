import Typography from "@mui/material/Typography";

const Viesdata=(val)=>{
    return (<div style={{ display: "flex", flexDirection: "column", width: "500px" }}>
    <img
      style={{ width: "100px", height: "100px",textAlign:'center', margin:'17px 10px' }}
      src={
                    val.gender === "Male"
                      ? "https://icon-library.com/images/avatar-icon-images/avatar-icon-images-4.jpg"
                      : "https://icons-for-free.com/iconfiles/png/512/female+person+user+woman+young+icon-1320196266256009072.png"
                  }
      alt="Live from space album cover"
    /> 
      <div className="lists">
       <Typography style={{color:'rgb(44, 48, 58)'}} component="div" variant="h5">
        Profile
      </Typography>
      <div className='list-row'><label style={{color:'orange',}}>Name</label>
        <p style={{color:'white'}}>{val.name}</p></div>  

        <div className='list-row'><label  style={{color:'orange'}}>Profile</label>
        <p style={{color:'white'}}>{val.profile}</p></div>  

        <div className='list-row'><label  style={{color:'orange'}}>Gender</label>
        <p style={{color:'white'}}>{val.gender}</p></div>  

        <div className='list-row'> <label  style={{color:'orange'}}>Email</label>
        <p style={{color:'white'}}>{val.email}</p></div>  

        <div className='list-row'> <label  style={{color:'orange'}}>Phone</label>
        <p style={{color:'white'}}>{val.phone}</p></div>  

        <div className='list-row'><label  style={{color:'orange'}}>Address</label>
        <p style={{color:'white'}}>{val.address}</p></div> 
      </div>
    </div>)
}
export default Viesdata